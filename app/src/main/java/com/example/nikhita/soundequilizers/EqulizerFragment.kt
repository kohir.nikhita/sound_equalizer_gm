package com.example.nikhita.soundequilizers

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlin.math.roundToInt


class EqulizerFragment : Fragment() {


    var includesId: ArrayList<String> =
        arrayListOf("inclBass", "inclMidrange", "inclTreble", "inclSurround")

    var includesTitle: ArrayList<String> =
        arrayListOf("Bass", "Midrange", "Treble", "Surround")

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        super.onCreateView(inflater, container, savedInstanceState)
        val view: View = inflater.inflate(R.layout.equlizer_fragment, container, false)


        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fun createequizerview() {

            for ((index, item) in includesId.withIndex()) {
                var imageListUp: Array<ImageView?>? = null
                imageListUp = arrayOfNulls<ImageView>(12)

                var imageListDown: Array<ImageView?>? = null
                imageListDown = arrayOfNulls<ImageView>(12)
                //include ids for widgets from four different includes in xml
                val includeName: View = view!!.findViewById(resources.getIdentifier(item, "id", activity!!.packageName))
                val includeValues: TextView = includeName.findViewById(R.id.bassvalue)
                val includeUpBars: View = includeName.findViewById(R.id.bassuplines)
                val includeDownBars: View = includeName.findViewById(R.id.bassdownlines)
                var volumeUp: ImageView? = includeName.findViewById<ImageView>(R.id.bassup)
                var volumeDown: ImageView? = includeName.findViewById<ImageView>(R.id.bassdown)

                var upbarsheight: Int = 0 //settng the bars height to zero to get them with event.y

                var count = 0
                includeValues.text = "${count}"

                includeUpBars.post {
                    upbarsheight = includeUpBars.measuredHeight // gets height of the upbars as the height is match parent in the layout
                }

                includeUpBars.setOnTouchListener { v, event ->
                    when (event?.action) {
                        MotionEvent.ACTION_MOVE -> {
                            if (event.y < upbarsheight && event.y > 0) { // 154 height of the uplines
                                var upValue = (event.y / 12).roundToInt() //154/12 = 12.4567 . rountInt + 1(values starting from 0 so +1)
                                Log.d("action moved", "${11 - upValue}") // convert 1234567 to 7654321 verticlly
                                var scrollValue = 11 - upValue
                                for (i in 0..11) {
                                    imageListUp[i]!!.alpha = 0.2F //setting the color value to 0.1F first
                                    imageListDown[i]!!.alpha = 0.2F
                                }
                                for (i in 0..scrollValue) { //and increasing it later
                                    imageListUp[i]!!.alpha = 0.8F
                                }
                                count = scrollValue + 1
                                if(count == 0){
                                    includeValues.text = "$count" //setting the count value if its less than 0
                                }else{
                                    includeValues.text = "+ $count"
                                }

                            }
                        }
                    }
                    true
                }

                includeDownBars.setOnTouchListener { v, event ->
                    when (event?.action) {
                        MotionEvent.ACTION_MOVE -> {
                            if (event.y < upbarsheight && event.y > 1) { // 154 height of the uplines
                                var upValue =
                                    (event.y / 12).roundToInt()
                                Log.d("Action Downbars", "${upValue}") //
                                var scrollValue = upValue
                                for (i in 0..11) {
                                    imageListUp[i]!!.alpha = 0.2F
                                    imageListDown[i]!!.alpha = 0.2F
                                }
                                for (i in 0..scrollValue - 2) {
                                    imageListDown[i]!!.alpha = 0.8F
                                }

                                if (scrollValue > 0) {
                                    count = 0 - (scrollValue - 1)
                                    includeValues.text = "$count"

                                }
                            }
                        }
                    }
                    true
                }


                includeName.findViewById<TextView>(R.id.bassText).text = includesTitle[index]

//               click listener for individual text values to set the count to zero
                includeValues.setOnClickListener { view ->
                    count = 0
                    includeValues.text = "${count}"

                    for (i in 0..11) {
                        imageListUp[i] = ImageView(context)
                        imageListUp[i] = includeUpBars.findViewById(
                            resources.getIdentifier(
                                "dividerup" + (i + 1),
                                "id",
                                activity!!.packageName  //fetching dividerup1 dividerup2.. and so on values
                            )
                        ) as ImageView
                        imageListUp[i]!!.alpha = 0.1F
                        imageListDown[i] = ImageView(context)
                        imageListDown[i] = includeDownBars.findViewById(
                            resources.getIdentifier(
                                "dividerdown" + (i + 1),
                                "id",
                                activity!!.packageName //fetching dividerdown1 dividerdown2.. and so on values
                            )
                        ) as ImageView
                        imageListDown[i]!!.alpha = 0.1F
                    }
                }


                for (i in 0..11) {
                    imageListUp[i] = ImageView(context)
                    imageListUp[i] = includeUpBars.findViewById(
                        resources.getIdentifier(
                            "dividerup" + (i + 1),   //fetching dividerup1 dividerup2.. and so on values
                            "id",
                            activity!!.packageName
                        )
                    ) as ImageView

                    imageListUp[i]!!.alpha = 0.1F
                    imageListUp[i]!!.setOnClickListener {
                        for (j in 0..11) {
                            imageListDown[j]!!.alpha = 0.1F
                            imageListUp[j]!!.alpha = 0.1F
                        }
                        for (j in 0..i) {
                            imageListUp[j]!!.alpha = 0.8F //fetching dividerdown1 dividerdown2.. and so on values
                            count = i + 1
                            includeValues.text = "+ ${count}"
                        }
                    }

                    imageListDown[i] = ImageView(context)
                    imageListDown[i] = includeDownBars.findViewById(
                        resources.getIdentifier(
                            "dividerdown" + (i + 1),
                            "id",
                            activity!!.packageName
                        )
                    ) as ImageView
                    imageListDown[i]!!.alpha = 0.1F
                    imageListDown[i]!!.setOnClickListener {
                        for (j in 0..11) {
                            imageListDown[j]!!.alpha = 0.1F
                            imageListUp[j]!!.alpha = 0.1F
                        }
                        for (j in 0..i) {
                            imageListDown[j]!!.alpha = 0.8F
                            count = -i
                            includeValues.text = "$count"
                        }
                    }
                }
//               click listener for volume up keys(bassup, midrange up, treble up, surround up)
                volumeUp!!.setOnClickListener {
                    if (count < 12) {
                        count++
                        if (count > 0) {
                            if(count == 0){
                                includeValues.text = "$count"
                            }else{
                                includeValues.text = "+ $count"
                            }
                            imageListUp[count - 1]!!.alpha = 0.8F
                        } else {
                            includeValues.text = "${count}"
                            imageListDown[Math.abs(count)]!!.alpha = 0.2F

                        }
                    }
                }

//               click listener for volume down keys(bassdown, midrange down, treble down, surround down)
                volumeDown!!.setOnClickListener { view ->
                    if (count != -12) {
                        count--
                        if (count < 0) {
                            includeValues.text = " $count"
                            imageListDown[Math.abs(count) - 1]!!.alpha = 0.8F
                        } else {
                            includeValues.text = "+ ${count}"
                            imageListUp[count]!!.alpha = 0.2F
                        }
                    }
                }

            }

        }

        view!!.findViewById<TextView>(R.id.resetButton).setOnClickListener {
            createequizerview()
        }
        createequizerview()
    }
}